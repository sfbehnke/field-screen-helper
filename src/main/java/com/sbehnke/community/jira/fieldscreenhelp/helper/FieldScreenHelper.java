package com.sbehnke.community.jira.fieldscreenhelp.helper;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A simple class for getting screens as simple objects
 */
@Named("JiraFieldScreenHelper")
public class FieldScreenHelper {

    @ComponentImport
    private final FieldScreenManager fieldScreenManager;

    @Inject
    public FieldScreenHelper(FieldScreenManager fieldScreenManager) {
        this.fieldScreenManager = fieldScreenManager;
    }

    public List<SimpleFieldScreen> getSimpleFieldScreens() {
        return getFieldScreens().stream()
                .map(SimpleFieldScreen::new)
                .collect(Collectors.toList());
    }

    private Collection<FieldScreen> getFieldScreens() {
        return fieldScreenManager.getFieldScreens();
    }
}
