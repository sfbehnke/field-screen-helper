package com.sbehnke.community.jira.fieldscreenhelp.resource;

import com.sbehnke.community.jira.fieldscreenhelp.helper.FieldScreenHelper;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Provides a GET endpoint at /rest/api/latest/field-screen-helper/screens
 */
@Named("JiraFieldScreenResource")
@Path("/screens")
public class FieldScreenResource {
    private final FieldScreenHelper fieldScreenHelper;

    @Inject
    public FieldScreenResource(FieldScreenHelper fieldScreenHelper) {
        this.fieldScreenHelper = fieldScreenHelper;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllScreens() {
        return Response.status(200)
                .entity(fieldScreenHelper.getSimpleFieldScreens())
                .build();
    }
}
