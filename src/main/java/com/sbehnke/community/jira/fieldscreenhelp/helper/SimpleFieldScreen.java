package com.sbehnke.community.jira.fieldscreenhelp.helper;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.util.NotNull;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * A simple bean to describe a field screen
 */
@XmlRootElement
public class SimpleFieldScreen implements Serializable {
    @XmlAttribute
    private Long id;
    @XmlAttribute
    private String name;
    @XmlAttribute
    private String description;

    public SimpleFieldScreen() {

    }

    public SimpleFieldScreen(@NotNull FieldScreen fieldScreen) {
        this.id = fieldScreen.getId();
        this.name = fieldScreen.getName();

        String description = fieldScreen.getDescription();
        if (description == null) {
            this.description = "";
        } else {
            this.description = description;
        }
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }
}
