### Field Screen Helper plugin
This plugin provides a simple REST endpoint that lists all Field Screens within a Jira system. This is valuable because the Jira platform does not provide this information.

This endpoint is at the following location: `<Jira-URL>/rest/field-screen-helper/1.0/screens`

This endpoint provides a JSON response in the following format:
```
[
  {
    "id": 19700,
    "name": "AAG: Task Tracking Default Screen",
    "description": ""
  },
  {
    "id": 19701,
    "name": "AAG: Task Tracking Epic Screen",
    "description": "A list of fields assigned to tabs."
  }
]
```
